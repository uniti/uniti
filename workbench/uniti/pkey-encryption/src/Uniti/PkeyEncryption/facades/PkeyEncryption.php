<?php namespace Uniti\PkeyEncryption\Facades;

use Illuminate\Support\Facades\Facade;

class PkeyEncryption extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
    	
    	return 'pkey-encryption';
    }
}