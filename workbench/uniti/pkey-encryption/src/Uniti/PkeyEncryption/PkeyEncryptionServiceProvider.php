<?php namespace Uniti\PkeyEncryption;

use Illuminate\Support\ServiceProvider;

class PkeyEncryptionServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('uniti/pkey-encryption');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['pkey-encryption'] = $this->app->share(function($app) {

			return new PkeyEncryption;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array(

			'pkey-encryption'
		);
	}

}
