<?php namespace Uniti\PkeyEncryption;

class PkeyEncryption {

	public static function encrypt($input, $cipher = MCRYPT_RIJNDAEL_128) {

		$key = sha1(microtime(true) . mt_rand(10000, 90000));

		$iv_size = mcrypt_get_iv_size($cipher, MCRYPT_MODE_CFB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

		return array(

			'encrypted' => base64_encode(mcrypt_encrypt($cipher, $key, $input, MCRYPT_MODE_CFB, $iv)),
			'iv'		=> $iv
		);
	}

	public static function decrypt($data, $key, $cipher = MCRYPT_RIJNDAEL_128) {

		$iv = $data['iv'];
		$data = $data['data'];

		return array(

			'decrypted' => mcrypt_decrypt($cipher, $key, $data, MCRYPT_MODE, CFB, $iv);
		);
	}
}