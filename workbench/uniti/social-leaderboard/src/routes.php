<?php

Route::group(array(

	'prefix' => 'api'
), function() {

	Route::group(array(

		'before' => 'auth|service:social-leaderboard'
	), function() {

		Route::post('social_leaderboard/leaderboard', array(

			'as'	=> 'social_leaderboard-leaderboard',
			'uses'	=> 'SocialLeaderboardController@leaderboard'
		));
	});
});