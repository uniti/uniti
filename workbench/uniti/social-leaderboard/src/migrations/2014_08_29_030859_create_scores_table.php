<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialScoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('s_social_leaderboard_scores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->bigInteger('user_id');
			$table->bigInteger('score');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('s_social_leaderboard_scores');
	}

}
