<?php namespace Uniti\SocialLeaderboard;

class SocialLeaderboardController extends BaseController {

	public function leaderboard() {

		$validator = Validator::make(Input::all(), array(

			'scope' => 'required'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'client_fault' => false
			), 422);
		}

		$users = array();
		switch (Input::get('scope')) {

			case 'FOLLOWS': {

				$users = Auth::user()->follows();
				break;
			}

			case 'FOLLOWERS': {

				$users = Auth::user()->followers;
				break;
			}

			default: {

				return Response::json(array(

					'client_fault' => false
				), 422);
			}
		}

		$scores = array();
		foreach ($users as $user) {

			$scores[] = $user->socialScore
		}

		if (!asort($scores)) {

			return Response::make(null, 500);
		}

		return Response::json(array(

			'leaderboard' => $scores
		), 200);
	}
}