<?php namespace Uniti\SocialLeaderboard;

use Illuminate\Support\ServiceProvider;

class SocialLeaderboardServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('uniti/social-leaderboard');
		include __DIR__.'/../../routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['social-leaderboard'] = $this->app->share(function($app) {

			return new SocialLeaderboard;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array(

			'social-leaderboard'
		);
	}

}
