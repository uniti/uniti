<?php namespace Uniti\SocialLeaderboard\Facades;

use Illuminate\Support\Facades\Facade;

class SocialLeaderboard extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
    	return 'social-leaderboard';

    }
}