<?php namespace Uniti\SocialLeaderboard;

class SocialScore extends \Eloquent {

	protected $table = 's_social_leaderboard_scores';

	protected $fillable = array(

		'user_id',
		'score'
	);
}
