<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(array(

	'prefix' => 'api'
), function()
{
	include ('routes/routes_auth.php');
	include ('routes/routes_content.php');
	include ('routes/routes_social.php');

	Route::post('search', array(

		'as'	=> 'search-search',
		'uses'	=> 'SearchController@search'
	));
});
