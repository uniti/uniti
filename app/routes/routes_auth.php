<?php

Route::group(array(

	'before' => 'guest'
), function(){

	Route::post('auth/register', array(

			'as' 	=> 'auth-register',
			'uses' 	=> 'AuthController@register'
		)
	);

	Route::post('auth/login', array(

			'as'	=> 'auth-login',
			'uses'	=> 'AuthController@login'
		)
	);
});

Route::group(array(
	
	'before' => 'auth'
), function(){

	Route::get('auth/user', array(

			'as'	=> 'auth-user',
			'uses'	=> 'AuthController@user'
		)
	);

	Route::post('auth/logout', array(
		
			'as'	=> 'auth-logout',
			'uses'	=> 'AuthController@logout'
		)
	);
});