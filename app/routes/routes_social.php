<?php

Route::group(array(


), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('social/follows/send', array(

			'as'	=> 'social-follows-send',
			'uses'	=> 'Social_FollowController@send'
		));

		Route::delete('social/follows/{id}/delete', array(

			'as'	=> 'social-followers',
			'uses'	=> 'Social_FollowController@delete'
		));
	});


	Route::get('social/follows/{username}', array(

		'as'	=> 'social-follows',
		'uses'	=> 'Social_FollowController@follows'
	));

	Route::get('social/followers/{username}', array(

		'as'	=> 'social-followers',
		'uses'	=> 'Social_FollowController@followers'
	));
});

Route::group(array(

	'name' => 'social-message'
), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('social/message/send', array(

			'as' 	=> 'social-message-send',
			'uses'	=> 'Social_MessageController@send'
		));

		Route::delete('social/message/{id}/delete', array(

			'as' 	=> 'social-message-delete',
			'uses'	=> 'Social_MessageController@delete'
		));
	});
});

Route::group(array(

	'name' => 'social-conversation'
), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('social/conversation/create', array(

			'as' 	=> 'social-conversation-create',
			'uses'	=> 'Social_ConversationController@create'
		));
	});
});
