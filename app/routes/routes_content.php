<?php

/**
 * Content events group
 */
Route::group(array(


), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('content/events/create', array(

			'as'	=> 'content-events-create',
			'uses'	=> 'Content_EventController@create'
		));

		Route::put('content/events/{id}/update', array(

			'as'	=> 'content-events-update',
			'uses'	=> 'Content_EventController@update'
		));

		Route::delete('content/events/{id}/delete', array(

			'as'	=> 'content-events-delete',
			'uses'	=> 'Content_EventController@delete'
		));
	});

	Route::get('content/events/{username}', array(

		'as' 	=> 'content-events-username',
		'uses'	=> 'Content_EventController@events'
	));

	Route::get('content/event/{id}', array(

		'as'	=> 'content-events-id',
		'uses'	=> 'Content_EventController@event'
	));
});

/**
 * Content pictures group
 */
Route::group(array(


), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('content/pictures/create', array(

			'as'	=> 'content-pictures-create',
			'uses'	=> 'Content_PictureController@create'
		));

		Route::put('content/pictures/{id}/update', array(

			'as'	=> 'content-pictures-update',
			'uses'	=> 'Content_PictureController@update'
		));

		Route::delete('content/pictures/{id}/delete', array(

			'as'	=> 'content-pictures-delete',
			'uses'	=> 'Content_PictureController@delete'
		));
	});

	Route::get('content/pictures/{username}', array(

		'as' 	=> 'content-pictures-username',
		'uses'	=> 'Content_PictureController@pictures'
	));

	Route::get('content/picture/{id}', array(

		'as'	=> 'content-pictures-id',
		'uses'	=> 'Content_PictureController@picture'
	));
});

/**
 * Content replies group
 */
Route::group(array(


), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('content/replies/create', array(

			'as'	=> 'content-replies-add',
			'uses'	=> 'Content_ReplyController@create'
		));

		Route::put('content/replies/{id}/update', array(

			'as'	=> 'content-replies-update',
			'uses'	=> 'Content_ReplyController@update'
		));

		Route::delete('content/replies/{id}/delete', array(

			'as'	=> 'content-replies-delete',
			'uses'	=> 'Content_ReplyController@delete'
		));
	});

	Route::get('content/replies/{content_id}', array(

		'as'	=> 'content-replies',
		'uses'	=> 'Content_ReplyController@replies'
	));
});

/**
 * Content stars group
 */
Route::group(array(


), function() {

	Route::group(array(

		'before' => 'auth'
	), function() {

		Route::post('content/stars/create', array(

			'as'	=> 'content-stars-add',
			'uses'	=> 'Content_StarController@create'
		));

		Route::delete('content/stars/{id}/delete', array(
	
			'as'	=> 'content-stars-delete',
			'uses'	=> 'Content_StarController@delete'
		));
	});

	Route::get('content/stars/{content_id}', array(

		'as'	=> 'content-stars',
		'uses'	=> 'Content_StarController@stars'
	));
});
