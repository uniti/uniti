<?php

class Social_ConversationController extends BaseController {

	public function create() {

		$validator = Validator::make(Input::all(), array(

			'participants'		=> /* validate serialized array */'required',
			'initial_message'	=> 'sometimes|min:2|max:255',
			'initial_flags'		=> 'required_with:initial_message'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		foreach (unserialize(Input::get('participants')) as $participant) {

			$validator = Validator::make(array(

				'participant' => $participant
			), array(

				'participant' => 'required|exists:users,id'
			));

			if ($validator->fails()) {

				return Response::json(array(

					'fields' => $validator->failed()
				), 400);
			}
		}

		$conversation = Social_Conversation::create(Input::all());

		if (!$conversation) {

			return Response::make(null, 500);
		}

		if (Input::has('initial_message')) {
			$message = Social_Message::create(array(

				'sender_id' 		=> Auth::user()->id,
				'conversation_id' 	=> $conversation->id,
				'content'			=> Input::get('initial_message'),
				'flags'				=> Input::get('initial_flags')
			));

			if (!$message) {

				return Response::make(null, 500);
			}
		}

		return Response::json(array(

			/* Why are >1 conversations returned? */ 'conversation' => $conversation->with('messages')->get()[0]
		), 201);	
	}
}