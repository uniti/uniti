<?php

class Social_MessageController extends BaseController {

	public function send() {

		$validator = Validator::make(Input::all(), array(

			'conversation_id'	=> 'sometimes|exists:conversations,id',
			'content'			=> 'required|min:2|max:255',
			'flags'				=> 'required'
		));

		if ($validator->fails()) {

				return Response::json(array(

					'fields' => $validator->failed()
				), 400);
		}

		$message = Social_Message::create(array(

			'sender_id' => Auth::user()->id
		) + Input::all());

		if (!$message) {

			return Response::make(null, 500);
		}

		return Response::json(array(

			'message' => $message
		), 201);
	}

	public function delete($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_replies,id',
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$message = Content_Message::find($id);

		if ($message->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$message->delete()) {

			return Response::make(null, 500);
		}

		return Response::make(null, 200);
	}
}