<?php

class Social_FollowController extends BaseController {

	public function send() {

		$validator = Validator::make(Input::all(), array(

			'person_b_username' => 'required|exists:users,username'
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$userB = User::where('username', '=', Input::get('person_b_username'))->first();

		$follow = Social_Follow::create(array(

			'person_a_id' => Auth::user()->id,
			'person_b_id' => $userB->id
		));

		if (!$follow) {

			return Response::make(null, 500);
		}

		$follow = Social_Follow::with('personA', 'personB')->find($follow->id);

		return Response::json(array(

			'follow' => $follow
		), 201);
	}

	public function follows($username) {

		$validator = Validator::make(array(

			'username' => $username
		), array(

			'username' => 'required|exists:users,username'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->messages()
			), 400);
		}

		$user = User::where('username', '=', $username)->first();

		return Response::json(array(

			'follows' => $user->follows
		));
	}

	public function followers($username) {

		$validator = Validator::make(array(

			'username' => $username
		), array(

			'username' => 'required|exists:users,username'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		$user = User::where('username', '=', $username)->first();

		return Response::json(array(

			'followers' => $user->followers
		));
	}

	public function delete($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:social_follows,id'
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$follow = Social_Follow::find($id);

		if ($conversation->person_a_id != Auth::user()->id && $conversation->person_b_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$follow->delete()) {

			return Response::make(null, 500);
		}

		return Response::make(null, 200);
	}
}