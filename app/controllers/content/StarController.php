<?php

class Content_StarController extends BaseController {

	public function create() {

		$content_type = substr(Input::get('parent_id'), 0, 1);

		$validation_string = 'required|exists:';
		switch ($content_type) {

			case 'E': {
					
				$validation_string .= 'content_events,content_id';
				break;
			}

			case 'P': {

				$validation_string .= 'content_pictures,content_id';
				break;
			}

			default: {
	
				return Response::make(null, 400);
			}
		}

		$validator = Validator::make(Input::all(), array(

			'parent_id'	=> $validation_string
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		$star = Content_Star::create(Input::all() + array(

			'creator_id' => Auth::user()->id
		));

		if (!$star) {

			return Response::make(null, 500);
		}

		$star = Content_Star::find($star->id);

		return Response::json(array(

			'star' => $star
		), 201);
	}

	public function delete($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_stars,id',
		));

		if ($validator->fails()) {

			return Response::make(null, 400);
		}

		$star = Content_Star::find($id);

		if ($star->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$star->delete()) {

			return Response::make(null, 500);
		}

		return Response::make(null, 200);
	}
}