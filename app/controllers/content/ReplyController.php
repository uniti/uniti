   <?php

class Content_ReplyController extends BaseController {

	public function create() {

		$content_type = substr(Input::get('parent_id'), 0, 1);

		$validation_string = 'required|exists:';
		switch ($content_type) {

			case 'E': {
					
				$validation_string .= 'content_events,content_id';
				break;
			}

			case 'P': {

				$validation_string .= 'content_pictures,content_id';
				break;
			}

			default: {
	
				return Response::make(null, 400);
			}
		}

		$validator = Validator::make(Input::all(), array(

			'parent_id'		=> $validation_string,
			'content'		=> 'required|min:4|max:255'
		));

		if ($validator->fails()) {

			if ($validator->messages()->has('parent_id')) {

				return Response::make(null, 404);
			}

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		$reply = Content_Reply::create(array(

			'creator_id' => Auth::user()->id
		) + Input::all());

		if (!$reply) {

			return Response::make(null, 500);
		}

		$reply = Content_Reply::with('creator')->find($reply->id);

		return Response::json(array(

			'reply' => $reply
		), 201);
	}

	public function update($id) {

		$validator = Validator::make(array(

			'id' => $id
		) + Input::all(), array(

			'id' 		=> 'required|exists:content_replies,id',
			'content'	=> 'sometimes|min:4|max:255'
		));

		if ($validator->fails()) {

			if ($validator->messages()->has('id')) {

				return Response::make(null, 404);
			}

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}
		
		$reply = Content_Reply::find($id);

		if ($reply->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$reply->update(Input::all())) {

			return Response::make(null, 500);
		}

		return Response::json(array(

			'reply' => $reply
		));
	}

	public function delete($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_replies,id',
		));

		if ($validator->fails()) {

			return Response::make(null, 400);
		}

		$reply = Content_Reply::find($id);

		if ($reply->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$reply->delete()) {

			return Response::make(null, 500);
		}

		return Response::make(null, 200);
	}
}