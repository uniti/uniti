<?php

class Content_EventController extends BaseController {

	public function create() {

		$validator = Validator::make(Input::all(), array(

			'title'		=> 'required|min:2|max:32',
			'story'		=> 'min:4|max:255'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}	

		$event = Content_Event::create(array(

			'creator_id' => Auth::user()->id
		) + Input::all());

		if (!$event) {

			return Response::make(null, 500);
		}

		$event->content_id = 'E' . $event->id;
		$event->save();

		$event = Content_Event::with('creator')->find($event->id);

		return Response::json(array(

			'event' => $event
		), 201);
	}

	public function events($username) {

		$validator = Validator::make(array(

			'username' => $username
		), array(

			'username' => 'required|exists:users,username'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		$user = User::where('username', '=', $username)->first();

		return Response::json(array(

			'events' => $user->events()->with('creator', 'pictures', 'replies', 'stars')->get()
		));
	}

	public function event($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_events,id'
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$event = Content_Event::with('pictures', 'replies', 'stars', 'creator')->find($id);

		return Response::json(array(

			'event' => $event
		));
	}

	public function update($id) {

		$validator = Validator::make(array(

			'id' => $id
		) + Input::all(), array(

			'id' 	=> 'required|exists:content_events,id',
			'title'	=> 'sometimes|min:2|max:32',
			'story'	=> 'sometimes|min:4|max:255'
		));

		if ($validator->fails()) {

			if ($validator->messages()->has('id')) {

				return Response::make(null, 404);
			}

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}
		
		$event = Content_Event::find($id);

		if ($event->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$event->update(Input::all())) {

			return Response::make(null, 500);
		}

		return Response::json(array(

			'event' => $event
		));
	}

	public function delete($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_events,id',
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$event = Content_Event::find($id);

		if ($event->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$event->delete()) {

			return Response::make(null, 500);
		}

		return Response::make(null, 200);
	}
}