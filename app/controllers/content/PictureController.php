<?php

class Content_PictureController extends BaseController {

	public function create() {

		$validator = Validator::make(Input::all(), array(

			'parent_id' => 'required|exists:content_events,content_id',
			'image' 	=> 'required|image',
			'caption' 	=> 'sometimes|min:4|max:255'
		));

		if ($validator->fails()) {

			if ($validator->messages()->has('parent_id')) {

				return Response::make9(null, 404);
			}

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}	

		$picture = Content_Picture::create(array(

			'creator_id' 	=> Auth::user()->id
		) + Input::all() + array(

			'extension' 	=> Input::file('image')->guessClientExtension()
		));

		if (!$picture) {

			return Response::make(null, 500);
		}

		$picture->content_id = 'P' . $picture->id;
		$picture->save();

		//save the image to storage
		Input::file('image')->move(
			$_SERVER['DOCUMENT_ROOT'] . '/storage/pictures', 
			$picture->id . '.' . Input::file('image')->guessClientExtension()
		);

		$picture = Content_Picture::with('creator')->find($picture->id);

		return Response::json(array(

			'picture' => $picture
		), 201);
	}

	public function pictures($username) {

		$validator = Validator::make(array(

			'username' => $username
		), array(

			'username' => 'required|exists:users,username'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		$user = User::where('username', '=', $username)->first();

		return Response::json(array(

			'pictures' => $user->pictures()->with('replies', 'stars')->get()
		));
	}

	public function picture($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_pictures,id'
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$picture = Content_Picture::with('replies', 'stars')->find($id);

		return Response::json(array(

			'picture' => $picture->with('replies', 'stars')->get()
		));
	}

	public function update($id) {

		$validator = Validator::make(array(

			'id' => $id
		) + Input::all(), array(

			'id' 		=> 'required|exists:content_pictures,id',
			'caption' 	=> 'sometimes|min:4|max:255'
		));

		if ($validator->fails()) {

			if ($validator->messages()->has('id')) {

				return Response::make(null, 404);
			}

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}
		
		$picture = Content_Picture::find($id);

		if ($picture->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		if (!$picture->update(Input::all())) {

			return Response::make(null, 500);
		}

		return Response::json(array(

			'picture' => $picture
		));
	}

	public function delete($id) {

		$validator = Validator::make(array(

			'id' => $id
		), array(

			'id' => 'required|exists:content_pictures,id',
		));

		if ($validator->fails()) {

			return Response::make(null, 404);
		}

		$picture = Content_Picture::find($id);

		if ($picture->creator_id != Auth::user()->id) {

			return Response::make(null, 403);
		}

		File::delete($_SERVER['DOCUMENT_ROOT'] . '/storage/pictures/' . $picture->id . '.' . $picture->extension);

		if (!$picture->delete()) {

			return Response::make(null, 500);
		}

		return Response::make(null, 200);
	}
}