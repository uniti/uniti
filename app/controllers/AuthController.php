<?php

class AuthController extends BaseController {

	public function register() {

		$validator = Validator::make(Input::all(), array(
			'email'				=> 'required|email|max:255|unique:users,email',
			'username'			=> 'required|min:2|max:32|alpha_dash|unique:users,username',
			'password'			=> 'required|min:8',
			'password_again'	=> 'required|same:password',
			'first_name'		=> 'required|min:2|max:32',
			'last_name'			=> 'required|min:2|max:32'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'fields' => $validator->failed()
			), 400);
		}

		$user = User::create(Input::except('password') + array(

			'password' => Hash::make(Input::get('password'))
		));

		if (!$user) {

			return Response::make(null, 500);
		}

		return Response::json(array(

			'user' => $user
		), 201);
	}

	public function login() {

		$validator = Validator::make(Input::all(), array(
			'username' 	=> 'required|max:32|min:2|alpha_dash|exists:users,username',
			'password'	=> 'required|min:8'
		));

		if ($validator->fails()) {

			return Response::json(array(

				'auth' 		=> false,
				'fields'	=> $validator->failed()
			), 400);
		}

		$user = User::where('username', '=', Input::get('username'))->first();

		if(Auth::attempt(array(

			'username' => Input::get('username'),
			'password' => Input::get('password')
		))) {

			if (Hash::needsRehash(Auth::user()->password)) {

				Auth::user()->password = Hash::make(Input::get('password'));

				if (!Auth::user()->save()) {

					return Response::make(null, 500);
				}
			}

			return Response::json(array(

				'auth' 	=> true,
				'user'	=> Auth::user()
			), 200);
		} else {

			return Response::json(array(

				'auth' 	=> false
			), 200);
		}
	}

	public function user() {

		return Response::json(array(

			'user' => Auth::user()
		));
	}

	public function logout() {
		
		Auth::logout();

		return Response::make(null, 200);
	}
}