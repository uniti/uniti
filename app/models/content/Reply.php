<?php

class Content_Reply extends Eloquent {
	
	protected $table = 'content_replies';

	protected $fillable = array(

		'creator_id',
		'parent_id',
		'content'
	);

	public function creator() {

		return $this->belongsTo('User', 'creator_id', 'id');
	}
}