<?php

class Content_Album extends Eloquent {

	protected $table = 'content_albums';

	protected $fillable = array(

		'creator_id',
		'title'
	);

	public function creator() {

		return $this->belongsTo('User', 'creator_id', 'id');
	}

	public function pictures() {

		return $this->hasMany('Content_Picture', 'album_id', 'id');
	}
}