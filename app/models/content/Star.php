<?php

class Content_Star extends Eloquent {

	protected $table = 'content_stars';

	protected $fillable = array(
		
		'creator_id',
		'parent_id'
	);

	public function creator() {

		return $this->belongsTo('User', 'creator_id', 'id');
	}
}