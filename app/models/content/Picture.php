<?php

class Content_Picture extends Eloquent {

	protected $table = 'content_pictures';

	protected $fillable = array(

		'content_id',
		'creator_id',
		'parent_id',
		'album_id',
		'caption',
		'extension'
	);

	public function creator() {

		return $this->belongsTo('User', 'creator_id', 'id');
	}

	public function replies() {
		
		return $this->hasMany('Content_Reply', 'parent_id', 'content_id');
	}

	public function stars() {

		return $this->hasMany('Content_Star', 'parent_id', 'content_id');
	}
}