<?php

class Content_Event extends Eloquent {

	protected $table = 'content_events';

	protected $fillable = array(

		'content_id',
		'creator_id',
		'title',
		'story'
	);

	public function creator() {

		return $this->belongsTo('User', 'creator_id', 'id');
	}

	public function pictures() {

		return $this->hasMany('Content_Picture', 'parent_id', 'content_id');
	}

	public function replies() {
		
		return $this->hasMany('Content_Reply', 'parent_id', 'content_id');
	}

	public function stars() {

		return $this->hasMany('Content_Star', 'parent_id', 'content_id');
	}
}