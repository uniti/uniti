<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	
	protected $table = 'users';

	protected $hidden 	= array(

		'password',
		'service_key',
		'remember_token'
	);

	protected $fillable = array(

		'email',
		'username',
		'password',
		'first_name',
		'last_name'
	);

	public function settings() {

		return $this->hasOne('Account_Settings', 'user_id', 'id');
	}

	public function events() {

		return $this->hasMany('Content_Event', 'creator_id', 'id');
	}

	public function pictures() {

		return $this->hasMany('Content_Picture', 'creator_id', 'id');
	}

	public function follows() {

		return $this->hasMany('Social_Follow', 'person_a_id', 'id');
	}

	public function followers() {

		return $this->hasMany('Social_Follow', 'person_b_id', 'id');
	}

	public function conversations() {

		return $this->hasMany('Social_Conversation', 'person_a_id', 'id')
			+ $this->hasMany('Social_Conversation', 'person_b_id', 'id');
	}

	public function socialScore() {

		return $this->hasOne('SocialScore', 'user_id', 'id');
	}
}
