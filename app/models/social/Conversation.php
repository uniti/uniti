<?php

class Social_Conversation extends Eloquent {
	
	protected $table = 'social_conversations';

	protected $fillable = array(

		'participants'
	);

	public function messages() {

		return $this->hasMany('Social_Message', 'conversation_id', 'id');
	}
}