<?php

class Social_Follow extends Eloquent {

	protected $table = 'social_follows';

	protected $fillable = array(

		'person_a_id',
		'person_b_id'
	);

	public function personA() {

		return $this->hasOne('User', 'id', 'person_a_id');
	}

	public function personB() {

		return $this->hasOne('User', 'id', 'person_b_id');
	}
}