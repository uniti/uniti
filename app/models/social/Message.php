<?php

class Social_Message extends Eloquent {

	protected $table = 'social_messages';

	protected $fillable = array(

		'sender_id',
		'conversation_id',
		'content',
		'flags'
	);

	public function sender() {

		return $this->hasOne('User', 'id', 'sender_id');
	}

	public function conversation() {

		return $this->belongsTo('Social_Conversation', 'conversation_id', 'id');
	}
}