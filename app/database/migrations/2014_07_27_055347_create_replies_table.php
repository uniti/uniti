<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepliesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content_replies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('creator_id');
			$table->string('parent_id');
			$table->string('content', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content_replies');
	}

}
