<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('email');
			$table->string('username', 32);
			$table->string('password');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('service_key', 32);
			$table->string('remember_token');
			$table->boolean('is_verified')->default(false);
			$table->boolean('is_banned')->default(false);
			$table->string('ban_reason');
			$table->date('unban_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
