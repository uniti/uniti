<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content_pictures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('content_id');
			$table->bigInteger('creator_id');
			$table->string('parent_id');
			$table->bigInteger('album_id');
			$table->string('caption', 255);
			$table->string('extension', 4);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content_pictures');
	}

}
