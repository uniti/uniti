<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content_events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('content_id');
			$table->bigInteger('creator_id');
			$table->string('title', 32);
			$table->string('story', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content_events');
	}

}
