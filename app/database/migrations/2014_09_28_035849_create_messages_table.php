<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('social_messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->bigInteger('sender_id');
			$table->bigInteger('conversation_id');
			$table->string('content');
			$table->string('flags');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social_messages');
	}

}
