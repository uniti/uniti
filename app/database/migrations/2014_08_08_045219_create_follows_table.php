<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('social_follows', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->bigInteger('person_a_id');
			$table->bigInteger('person_b_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social_follows');
	}

}
